package challenge;

import java.math.BigDecimal;

import challenge.triangle.Side;

public final class TestData {
	private TestData() throws IllegalAccessException {
		throw new IllegalAccessException("No instance protection");
	}

	public static Side createValidSide() {
		return Side.from(BigDecimal.TEN);
	}
}
