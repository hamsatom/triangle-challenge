package challenge.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import challenge.triangle.Side;

public class InputMock implements InputStrategy {

	private final List<Side> sides;
	private int position;

	public InputMock(Side firstSide, Side... moreSides) {
		sides = new ArrayList<>(moreSides.length + 1);
		sides.add(firstSide);
		sides.addAll(Arrays.asList(moreSides));
	}

	@Nonnull
	@Override
	public Side readSide() {
		int index = position++ % sides.size();
		return sides.get(index);
	}
}
