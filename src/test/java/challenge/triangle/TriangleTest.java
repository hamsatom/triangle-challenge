package challenge.triangle;

import org.testng.Assert;

abstract class TriangleTest {
	Triangle triangle;

	void compareName(String expectedName) {
		Assert.assertEquals(triangle.getName(), expectedName, "Wrong triangle name");
	}
}
