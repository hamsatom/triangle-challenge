package challenge.triangle;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import challenge.TestData;

public class IsoscelesTriangleTest extends TriangleTest {

	@BeforeMethod
	public void setUp() {
		triangle = new IsoscelesTriangle(TestData.createValidSide(), TestData.createValidSide(), TestData.createValidSide());
	}

	@Test
	public void testGetName() {
		compareName("Isosceles");
	}
}