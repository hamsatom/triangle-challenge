package challenge.triangle;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.TestData;
import challenge.input.InputMock;
import challenge.input.InputStrategy;

public class TriangleFactoryTest {

	@Test
	public void testCreateTriangleValid() {
		Side side = TestData.createValidSide();
		Assert.assertNotNull(TriangleFactory.createTriangle(side, side, side));
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testCreateTriangleSideInvalid() {
		Side side1 = Side.from(BigDecimal.ONE);
		Side side10 = Side.from(BigDecimal.TEN);
		TriangleFactory.createTriangle(side1, side10, side1);
	}

	@Test(priority = 1)
	public void testCreateTriangleSideEquilateral() {
		Side side = TestData.createValidSide();
		Triangle triangle = TriangleFactory.createTriangle(side, side, side);
		Assert.assertTrue(triangle instanceof EquilateralTriangle, "Not an equilateral triangle");
	}

	@Test(priority = 1)
	public void testCreateTriangleSideIsosceles() {
		Side side = Side.from(BigDecimal.TEN);
		Side differentSide = Side.from(BigDecimal.ONE);
		Triangle triangle = TriangleFactory.createTriangle(side, differentSide, side);
		Assert.assertTrue(triangle instanceof IsoscelesTriangle, "Not an isosceles triangle");
	}

	@Test(priority = 1)
	public void testCreateTriangleSideScalene() {
		Side side1 = Side.from(BigDecimal.ONE);
		Side side10 = Side.from(BigDecimal.TEN);
		Side side11 = Side.from("11");
		Triangle triangle = TriangleFactory.createTriangle(side10, side1, side11);
		Assert.assertTrue(triangle instanceof ScaleneTriangle, "Not a scalene triangle");
	}

	@Test(priority = 2, expectedExceptions = IllegalArgumentException.class)
	public void testCreateTriangleInputInvalid() {
		Side side1a = Side.from(BigDecimal.ONE);
		Side side1b = Side.from(BigDecimal.ONE);
		Side side11 = Side.from("11");
		InputStrategy input = new InputMock(side1a, side1b, side11);
		TriangleFactory.createTriangle(input);
	}

	@Test(priority = 2)
	public void testCreateTriangleInputEquilateral() {
		InputStrategy input = new InputMock(TestData.createValidSide());
		Triangle triangle = TriangleFactory.createTriangle(input);
		Assert.assertTrue(triangle instanceof EquilateralTriangle, "Not an equilateral triangle");
	}

	@Test(priority = 2)
	public void testCreateTriangleInputIsosceles() {
		Side side = Side.from(BigDecimal.TEN);
		Side differentSide = Side.from(BigDecimal.ONE);
		InputStrategy input = new InputMock(side, differentSide);
		Triangle triangle = TriangleFactory.createTriangle(input);
		Assert.assertTrue(triangle instanceof IsoscelesTriangle, "Not an isosceles triangle");
	}

	@Test(priority = 2)
	public void testCreateTriangleInputScalene() {
		Side side1 = Side.from(BigDecimal.ONE);
		Side side10 = Side.from(BigDecimal.TEN);
		Side side11 = Side.from("11");
		InputStrategy input = new InputMock(side1, side10, side11);
		Triangle triangle = TriangleFactory.createTriangle(input);
		Assert.assertTrue(triangle instanceof ScaleneTriangle, "Not a scalene triangle");
	}
}