package challenge.triangle;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import challenge.TestData;

public class EquilateralTriangleTest extends TriangleTest {

	@BeforeMethod
	public void setUp() {
		triangle = new EquilateralTriangle(TestData.createValidSide(), TestData.createValidSide(), TestData.createValidSide());
	}

	@Test
	public void testGetName() {
		compareName("Equilateral");
	}
}