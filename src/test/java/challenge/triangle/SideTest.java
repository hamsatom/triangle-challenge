package challenge.triangle;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SideTest {

	@Test
	public void testFromStringValid() {
		Assert.assertNotNull(Side.from("20"));
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromStringText() {
		Assert.assertNotNull(Side.from("89g"));
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromStringBlank() {
		Assert.assertNotNull(Side.from(" "));
	}

	@Test
	public void testFromStringWithSpace() {
		Assert.assertNotNull(Side.from("8 "));
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromStringNegative() {
		Side.from("-15.4");
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromStringZero() {
		Side.from("0.0");
	}

	@Test
	public void testFromBigDecimalValid() {
		Assert.assertNotNull(Side.from(BigDecimal.ONE));
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromBigDecimalNegative() {
		Side.from(BigDecimal.TEN.negate());
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testFromBigDecimalZero() {
		Side.from(BigDecimal.ZERO);
	}

	@Test(priority = 1)
	public void testIsNotLongerThanAllCombinedShorter() {
		Side shorter = Side.from(BigDecimal.ONE);
		Side longer = Side.from(BigDecimal.TEN);
		Side equal = Side.from(BigDecimal.ONE);
		Assert.assertTrue(shorter.isNotLongerThanAllCombined(longer, equal), "Shorter side behaves as longer");
	}

	@Test(priority = 1)
	public void testIsNotLongerThanAllCombinedShorterEqual() {
		Side shorter = Side.from(BigDecimal.ONE);
		Side longer = Side.from(BigDecimal.TEN);
		Side equal = Side.from(BigDecimal.valueOf(11L));
		Assert.assertTrue(equal.isNotLongerThanAllCombined(longer, shorter), "Equal side behaves as shorter");

	}

	@Test(priority = 1)
	public void testIsNotLongerThanAllCombinedShorterLonger() {
		Side shorter = Side.from(BigDecimal.ONE);
		Side longer = Side.from(BigDecimal.TEN);
		Side equal = Side.from(BigDecimal.ONE);
		Assert.assertFalse(longer.isNotLongerThanAllCombined(shorter, equal), "Longer side behaves as shorter");
	}

	@Test(priority = 1)
	public void testIsEquallyLongAsAllEqual() {
		Side side10a = Side.from("10");
		Side side10b = Side.from(BigDecimal.TEN);
		Side side10c = Side.from("10.0");
		Assert.assertTrue(side10a.isEquallyLongAsAll(side10b, side10c), "Equal side behaves as different");
	}

	@Test(priority = 1)
	public void testIsEquallyLongAsAllDifferent() {
		Side side10 = Side.from("10");
		Side side1 = Side.from(BigDecimal.ONE);
		Side side3 = Side.from("3.5");
		Assert.assertFalse(side10.isEquallyLongAsAll(side1, side3), "Different side behaves as equal");
	}

	@Test(priority = 1)
	public void testIsEquallyLongAsAllSomeEqual() {
		Side side10a = Side.from("10");
		Side side10b = Side.from(BigDecimal.TEN);
		Side side3 = Side.from("3.5");
		Assert.assertFalse(side10a.isEquallyLongAsAll(side10b, side3), "Different side behaves as equal");
	}
}