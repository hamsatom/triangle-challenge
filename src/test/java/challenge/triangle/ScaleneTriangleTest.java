package challenge.triangle;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import challenge.TestData;

public class ScaleneTriangleTest extends TriangleTest {

	@BeforeMethod
	public void setUp() {
		triangle = new ScaleneTriangle(TestData.createValidSide(), TestData.createValidSide(), TestData.createValidSide());
	}

	@Test
	public void testGetName() {
		compareName("Scalene");
	}
}