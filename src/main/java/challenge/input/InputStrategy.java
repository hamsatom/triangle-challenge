package challenge.input;

import javax.annotation.Nonnull;

import challenge.triangle.Side;

/*I decided to use interface for input because this way I can mock input for tests.
It also allows me to create more implementations later without having to change the logic that operates with input */

/**
 * Common interface for input
 * <p>Encapsulates multiple strategies for processing input</p>
 */
@FunctionalInterface
public interface InputStrategy {

	/**
	 * Reads length from input and creates {@link Side}
	 *
	 * @return valid {@code Side}
	 */
	@Nonnull
	Side readSide();
}
