package challenge.input;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.annotation.Nonnull;

import challenge.triangle.Side;

/**
 * Input from {@link InputStream}
 */
public class InputStreamInput implements InputStrategy {

	private final BufferedReader reader;

	public InputStreamInput(@Nonnull InputStream inputStream) {
		reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream)));
	}

	@Nonnull
	@Override
	public Side readSide() {
		String line = null;
		Side side = null;
		while (side == null) {
			System.out.println("Type length of side and press enter");
			try {
				line = reader.readLine();
				side = Side.from(line);
			} catch (IllegalArgumentException e) {
				System.err.println("Invalid side length '" + line + "'. Length of side must be a positive number.");
			} catch (IOException e) {
				System.err.println("Unexpected error while reading");
			}
		}
		return side;
	}
}
