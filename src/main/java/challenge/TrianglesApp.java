package challenge;

import challenge.input.InputStrategy;
import challenge.input.InputStreamInput;
import challenge.triangle.Triangle;
import challenge.triangle.TriangleFactory;

/**
 * Example program that determines the type of a given triangle based on side lengths
 * <p>Takes length of 3 sides from input and outputs the type of the triangle</p>
 */
public class TrianglesApp {

	/**
	 * Reads 3 valid lengths of sides from standard input.
	 * Than outputs the type of given triangle to standard output.
	 * All errors are printed to standard error output while all instructions are printed to standard output.
	 */
	public static void main(String[] args) {
		InputStrategy input = new InputStreamInput(System.in);
		try {
			Triangle triangle = TriangleFactory.createTriangle(input);
			System.out.println("Triangle is " + triangle.getName());
		} catch (IllegalArgumentException e) {
			System.err.println("Invalid triangle. Triangle must satisfy triangle inequality.");
		}
	}
}
