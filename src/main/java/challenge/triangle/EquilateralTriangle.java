package challenge.triangle;

import javax.annotation.Nonnull;

/**
 * Represents equilateral triangle
 * <p>Equilateral triangle is a special case of a {@link Triangle}.
 * A triangle is equilateral if and only if all of it's sides have the same length.</p>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Equilateral_triangle">Wikipedia</a>"
 */
class EquilateralTriangle extends Triangle {

	EquilateralTriangle(@Nonnull Side side1, @Nonnull Side side2, @Nonnull Side side3) {
		super(side1, side2, side3);
	}

	@Nonnull
	@Override
	public String getName() {
		return "Equilateral";
	}
}
