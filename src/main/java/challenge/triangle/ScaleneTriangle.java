package challenge.triangle;

import javax.annotation.Nonnull;

/**
 * Represents scalene triangle
 * <p>Scalene triangle is a special case of a {@link Triangle}.
 * A scalene triangle has all of it's side with a different length.</p>
 */
class ScaleneTriangle extends Triangle {

	ScaleneTriangle(@Nonnull Side side1, @Nonnull Side side2, @Nonnull Side side3) {
		super(side1, side2, side3);
	}

	@Nonnull
	@Override
	public String getName() {
		return "Scalene";
	}
}
