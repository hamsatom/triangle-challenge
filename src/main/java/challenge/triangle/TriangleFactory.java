package challenge.triangle;

import javax.annotation.Nonnull;

import challenge.input.InputStrategy;

/* I decided to use factory pattern to get a specific instance of a triangle. It's because I have different triangle implementations
that I want to get according to some parameters which makes it suitable for factory pattern.
This also gives me opportunity to throw an exception if triangle cannot be constructed. */

/**
 * Creates specific instances of {@link Triangle}
 * <p>Should be used to get instance of a {@code Triangle}. The instances returned are instances of a specific {@code Triangle} type in regards to characteristics of sides
 * provided</p>
 */
public final class TriangleFactory {

	private TriangleFactory() throws IllegalAccessException {
		throw new IllegalAccessException("Initializing factory");
	}

	/**
	 * Creates specific {@link Triangle} according to side lengths taken from input
	 *
	 * @param input Source of side lengths
	 * @return Specific {@code Triangle}
	 * @throws IllegalArgumentException If triangle cannot be created from provided {@code Side}s
	 */
	@Nonnull
	public static Triangle createTriangle(@Nonnull InputStrategy input) throws IllegalArgumentException {
		Side side1 = input.readSide();
		Side side2 = input.readSide();
		Side side3 = input.readSide();
		return createTriangle(side1, side2, side3);
	}

	/**
	 * Creates specific {@link Triangle} according to characteristics of provided {@code Side}s
	 *
	 * @param side1 First side of the {@code Triangle}
	 * @param side2 Second side of the {@code Triangle}
	 * @param side3 Third side of the {@code Triangle}
	 * @return Specific {@code Triangle}
	 * @throws IllegalArgumentException If triangle cannot be created from provided {@code Side}s
	 */
	@Nonnull
	public static Triangle createTriangle(@Nonnull Side side1, @Nonnull Side side2, @Nonnull Side side3) throws IllegalArgumentException {
		if (!isTriangleInequalityPreserved(side1, side2, side3)) {
			throw new IllegalArgumentException("Length of the sides must fulfil the triangle inequality");
		} else if (side1.isEquallyLongAsAll(side2, side3)) {
			return new EquilateralTriangle(side1, side2, side3);
		} else if (side1.isEquallyLongAsAll(side2) || side1.isEquallyLongAsAll(side3) || side2.isEquallyLongAsAll(side3)) {
			return new IsoscelesTriangle(side1, side2, side3);
		} else {
			return new ScaleneTriangle(side1, side2, side3);
		}
	}

	/**
	 * Triangle inequality is preserved when two sides are longer or equally long as the third side.
	 * If the sides are equal we get degenerated triangle which is still a triangle.
	 *
	 * @return {@code true} if the triangle inequality is not violated, false otherwise
	 * @see <a href="https://en.wikipedia.org/wiki/Triangle_inequality">Triangle inequality definition</a>
	 */
	private static boolean isTriangleInequalityPreserved(Side side1, Side side2, Side side3) {
		return side1.isNotLongerThanAllCombined(side2, side3) && side2.isNotLongerThanAllCombined(side1, side3) && side3.isNotLongerThanAllCombined(side1, side2);
	}
}
