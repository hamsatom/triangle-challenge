package challenge.triangle;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.annotation.Nonnull;

/*I decided to represent Triangle's side by an object because this way I can validate the value directly on the object.
It will also make it easier to add additional functionality for Side which would be more difficult if the Side was represented by just a number.
Moreover, I decided to use big decimal because there was no specification how long the sides could be.*/

/**
 * Represents one side of a {@link Triangle}. Consists of length.
 * <p>Two sides are considered equal if their lengths are equal.</p>
 */
public class Side {

	// Big decimal because there was nothing mentioned about length restriction in the task assignment
	private final BigDecimal length;

	/**
	 * Private constructor, use factory methods to create a new instance
	 *
	 * @see #from(String)
	 * @see #from(BigDecimal)
	 */
	private Side(BigDecimal length) {
		this.length = length;
	}

	/**
	 * Create new {@code Side} from length in {@code String}
	 *
	 * @param length length of the new side
	 * @return valid {@code Side}
	 * @throws IllegalArgumentException if the length is not a positive number
	 */
	@Nonnull
	public static Side from(@Nonnull String length) throws IllegalArgumentException {
		length = length.trim();
		try {
			return from(new BigDecimal(length));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Length of side must be a number");
		}
	}

	/**
	 * Create new {@code Side} from length in {@code BigDecimal}
	 *
	 * @param length length of the new side
	 * @return valid {@code Side}
	 * @throws IllegalArgumentException if the length is not a positive number
	 */
	@Nonnull
	public static Side from(@Nonnull BigDecimal length) throws IllegalArgumentException {
		if (length.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Length of side must be a positive number");
		}
		return new Side(length);
	}

	/**
	 * Compares if the length of this {@code Side} is equal to the length of all the {@code Side}s provided.
	 *
	 * @param firstSide First {@code Side} being compared
	 * @param moreSides 0 to N more {@code Side}s for comparision
	 * @return {@code true} only if the length of this {@code Side} is equal to the length of each {@code Side} provided, {@code false} otherwise
	 */
	public boolean isNotLongerThanAllCombined(@Nonnull Side firstSide, @Nonnull Side... moreSides) {
		BigDecimal sum = firstSide.length;
		for (Side side : moreSides) {
			sum = sum.add(side.length);
		}
		return length.compareTo(sum) <= 0;
	}

	/**
	 * Compares if the length of this {@code Side} is greater or equal to sum of all lengths of all the {@code Side}s provided.
	 *
	 * @param firstSide First {@code Side} being compared
	 * @param moreSides 0 to N more {@code Side}s for comparision
	 * @return {@code true} only if the length of this {@code Side} is greater or equal to the sum of lengths of each {@code Side} provided, {@code false} otherwise
	 */
	public boolean isEquallyLongAsAll(@Nonnull Side firstSide, @Nonnull Side... moreSides) {
		// Compare to instead of equals because 2.0 is not equal to 2
		return length.compareTo(firstSide.length) == 0 && Arrays.stream(moreSides)
				.allMatch(side -> length.compareTo(side.length) == 0);
	}
}
