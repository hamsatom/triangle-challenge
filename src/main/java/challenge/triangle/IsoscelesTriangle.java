package challenge.triangle;

import javax.annotation.Nonnull;

/**
 * Represents isosceles triangle
 * <p>Isosceles triangle is a special case of a {@link Triangle}.
 * An isosceles triangle has exactly 2 sides of the same length.</p>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Isosceles_triangle">Wikipedia</a>"
 */
class IsoscelesTriangle extends Triangle {

	IsoscelesTriangle(@Nonnull Side side1, @Nonnull Side side2, @Nonnull Side side3) {
		super(side1, side2, side3);
	}

	@Nonnull
	@Override
	public String getName() {
		return "Isosceles";
	}
}
