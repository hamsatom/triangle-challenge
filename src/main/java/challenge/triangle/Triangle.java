package challenge.triangle;

import javax.annotation.Nonnull;

/*I decided to represent all possible triangles by objects because that makes the code easily extendable in the future.
At first I was thinking about having enums for the types. While enums would be more than okay for now, they would
not be sufficient if we wanted to implement different behaviour for each kind of triangle.
This save us from a potential refactor later on*/

/**
 * Base class of a triangle
 * <p>Triangle consists of 3 {@link Side}s</p>
 */
public abstract class Triangle {

	private final Side side1;
	private final Side side2;
	private final Side side3;

	/**
	 * Package private, use {@link TriangleFactory} to get instance
	 *
	 * @see TriangleFactory
	 */
	Triangle(@Nonnull Side side1, @Nonnull Side side2, @Nonnull Side side3) {
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}

	/* Instead of returning String I could return enum of triangle types. But since I have a class for each type
		I see no point in returning enum. Any functionality that requires specific triangle type can be called on corresponding class*/

	/**
	 * Provides the name of the specific triangle
	 *
	 * @return Specific name of the triangle
	 */
	@Nonnull
	public abstract String getName();
}
