[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/triangle-challenge/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/triangle-challenge/commits/master) [![coverage report](https://gitlab.fel.cvut.cz/hamsatom/triangle-challenge/badges/master/coverage.svg)](https://gitlab.fel.cvut.cz/hamsatom/triangle-challenge/commits/master)
# Triangle challenge
**Author:** Tomáš Hamsa
## Assignment
Write a program that will determine the type of a triangle. It should take the lengths of the triangle's three sides as input, and return whether the triangle is equilateral, isosceles or scalene.  
We are looking for solutions that showcase problem solving skills and structural considerations that can be applied to larger and potentially more complex problem domains. Pay special attention to tests, readability of code and error cases.
## Solution outline
My solution is an overkill for just determining type of a triangle. But it will server quite well if we want to work further with triangles.   
My goal was to create easily extendable program. I wanted to make it simple to add additional functionality to different triangles in the future. As well as make possible to add additional forms of input. And also separate independent program parts such as input, logic and models.     
More implementation decisions are described in the code. I used factory pattern to obtains specific triangles based on sides provided. And strategy pattern to be able to mock input. Length of a side is  represented by BigDecimal because no value restriction was mentioned in the assignment.  
Program outputs instructions and result to standard output while taking input from standard input. All errors are printed to standard error output.  
Project uses GitLab CI to run tests. Technologies used are Maven, Java8 and TestNG for testing. Reason being I'm familiar with these technologies.
## Structure
![Class diagram](classDiagram.png)
 